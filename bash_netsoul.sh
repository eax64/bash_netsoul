#!/bin/bash
## bash_netsoul.sh for  in /home/soules_k/dev
## 
## Made by Kevin Soules
## Login   <soules_k@epitech.net>
## 
## Started on  Mon Feb 24 13:58:07 2014 Kevin Soules
## Last update Mon Jan 19 18:59:00 2015 eax
##


login=""
password=""
host="blah"
client="BashNsClient 0.2"

if [ -z $login ]
then
    read -p "login: " login
fi
if [ -z $password ]
then
    stty -echo
    read -p "password: " password
    echo
    stty echo
fi

NSSERVERADDR="ns-server.epitech.eu"
NSPORT=4242

function wait_until_can_connect {
    first=1
    until timeout 10 bash -c "cat < /dev/null > /dev/tcp/$NSSERVERADDR/$NSPORT" 2> /dev/null;
    do
	if [ $first -eq 1 ]
	then
	    first=0
	    echo -n "Fail to (re)create the socket."
	else
	    echo -n "."
	fi
	sleep 2
    done
    echo ""
}

function connect_ns {
    echo "=== Starting connection ==="

    wait_until_can_connect;
    
    exec 42<>/dev/tcp/$NSSERVERADDR/$NSPORT;
    sleep 0.2

    read -u 42 line
    line=$(echo $line | cut -d" " -f 3-5)
    IFS=" " read -a array <<< $line
    md5="${array[0]}"
    ip="${array[1]}"
    port="${array[2]}"
    hsh=$(echo -n $md5-$ip/$port$password | md5sum | cut -d" " -f 1)

    line="auth_ag ext_user none none"
    echo $line >&42
    echo "Sent: $line"  >&2
    sleep 0.2;

    read -u 42 line
    echo "Received: $line" >&2
    sleep 0.2

    line="ext_user_log $login $hsh $host BashNsClient"
    echo $line >&42
    echo "Sent: $line" >&2
    read -u 42 line
    sleep 0.2

    echo "Received: $line" >&2
}

connect_ns;

cnt=0
last_incr=`date +%s`
while true;
do
    while [ $((`date +%s` - $last_incr)) -lt 30 ];
    do
	if read -t 1 -u 42 line
	then
	    echo -e "Received:\033[032m $line \033[0m"
	fi
	if read -t 1 -u 0 line
	then
	    echo "Sending msg."
	    echo "$line" >&42
	fi
    done
    echo "Sending pong" >&2
    echo "state actif:`date +%s`" >&42
    cnt=$(($cnt+1))
    last_incr=`date +%s`
    if [ $cnt -gt 40 ]
    then
	echo "=== Killing connection ===" >&2
	exec 42<&-
	sleep 0.2
	connect_ns;
	cnt=0
    fi
done
